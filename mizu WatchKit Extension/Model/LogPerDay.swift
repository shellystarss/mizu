//
//  LogPerDay.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 20/08/21.
//

import SwiftUI

struct LogPerDay{
    let date: Date
    let waterLogs: [WaterLog]
}
