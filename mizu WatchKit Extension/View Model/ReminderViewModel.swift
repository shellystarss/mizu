//
//  ReminderViewModel.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 20/08/21.
//

import SwiftUI
import UserNotifications

class ReminderViewModel: ObservableObject{
    private var reqAuth = false
    
    func convertTime()->String{
        var time = ReminderViewModel().getTimeInterval()
        if(time=="20m"){
            time="20 minutes"
        }else if(time=="30m"){
            time="30 minutes"
        }else if(time=="45m"){
            time="45 minutes"
        }else if(time=="1h"){
            time="1 hour"
        }else if(time=="1h30m"){
            time="1 hour 30 minutes"
        }else if(time=="2h"){
            time="2 hours"
        }
        return time
    }
    
    func getPendingNotification(){
        var displayString = "Current Pending Notifications "
        UNUserNotificationCenter.current().getPendingNotificationRequests {
            (requests) in
            displayString += "count:\(requests.count)\t"
            for request in requests{
                displayString += request.identifier + "\t"
            }
            print(displayString)
        }
    }
    
    func setWaterReminderStatus(isOn: Bool){
        if(isOn){
            if(!reqAuth){
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
                    if success {
                        WKExtension.shared().registerForRemoteNotifications()
                    } else if let error = error {
                        print(error.localizedDescription)
                    }
                }
                reqAuth = true
            }
            makeNotification()
        }else{
            removeAllNotification()
        }
        getPendingNotification()
        UserDefaults.standard.setValue(isOn, forKey: "waterReminder")
    }
    
    func removeAllNotification(){
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    func makeNotification(){
        
        removeAllNotification()
        
        var time1 = convertTime12To24(time: getStartTime())
         let time2 = convertTime12To24(time: getEndTime())
         let interval = getTimeInterval()
        
         var intervalMin = 1
         if(interval=="20m"){
            intervalMin = 20
         }else if(interval=="30m"){
            intervalMin = 30
         }else if(interval=="45m"){
            intervalMin = 45
         }else if(interval=="1h"){
            intervalMin = 60
         }else if(interval=="1h30m"){
            intervalMin = 90
         }else if(interval=="2h"){
            intervalMin = 120
         }
         
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "HH:mm"
         
         let difference = Calendar.current.dateComponents([.hour, .minute], from: time1, to: time2)
         let formatString = String(format: "%2ld:%2ld", difference.hour!, difference.minute!)
         
         let formatDate = dateFormatter.date(from: formatString)
         let hour = Calendar.current.component(.hour, from: formatDate!)
         let minute = Calendar.current.component(.minute, from: formatDate!)
         let diffInt = hour * 60 + minute
         
         let loop = diffInt / intervalMin
         
         let intervalSec = intervalMin * 60
         
         for i in 0..<loop{
            time1 = time1 + Double(intervalSec)
            
            let content = UNMutableNotificationContent()
            content.title = "Time to drink water!"
            content.subtitle = "Keep your body hydrated"
            content.categoryIdentifier = "myCategory"
            content.sound = UNNotificationSound.default
            
            let dateComponents =  Calendar.current.dateComponents([.hour, .minute], from: time1)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)

            // choose a random identifier
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            
            // add our notification request
            UNUserNotificationCenter.current().add(request)
            
         }
        
        
        
    }
    
    func convertTime12To24(time: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        dateFormatter.dateFormat = "h:mma"
        let date12 = dateFormatter.date(from: time)
        
        dateFormatter.dateFormat = "HH:mm"
        let date24 = dateFormatter.string(from: date12 ?? Date())
        
        let date = dateFormatter.date(from: date24)!
        
        return date
    }
    
    func checkReminderRange() -> Bool {
        let todaysDate = Date()
        let startTime = convertTime12To24(time: getStartTime())
        let endTime = convertTime12To24(time: getEndTime())
        
        var startComponents = Calendar.current.dateComponents([.hour, .minute], from: startTime)
        
        var endComponents = Calendar.current.dateComponents([.hour, .minute], from: endTime)
        
        let nowComponents = Calendar.current.dateComponents([.month, .day, .year], from: todaysDate)
        
        startComponents.year  = nowComponents.year
        startComponents.month = nowComponents.month
        startComponents.day   = nowComponents.day

        endComponents.year  = nowComponents.year
        endComponents.month = nowComponents.month
        endComponents.day   = nowComponents.day
        
        let startDate = Calendar.current.date(from: startComponents)
        let endDate = Calendar.current.date(from: endComponents)
        
        
        let isInRange = todaysDate > startDate! && todaysDate < endDate!
        
        return isInRange
    }
    
    func getWaterReminderStatus()->Bool{
        let waterReminder = UserDefaults.standard.bool(forKey: "waterReminder")
        
        return waterReminder
    }
    
    func setTimeInterval(interval: String){
        var time = ""
        if(interval=="20 minutes"){
            time = "20m"
        }else if(interval=="30 minutes"){
            time = "30m"
        }else if(interval=="45 minutes"){
            time = "45m"
        }else if(interval=="1 hour"){
            time = "1h"
        }else if(interval=="1 hour 30 minutes"){
            time = "1h30m"
        }else if(interval=="2 hours"){
            time = "2h"
        }
        
        UserDefaults.standard.setValue(time, forKey: "timeInterval")
    }
    
    func getTimeInterval()->String{
        let timeInterval = UserDefaults.standard.string(forKey: "timeInterval")
        
        return timeInterval ?? "Not set"
    }
    
    func setStartTime(time: String){
        UserDefaults.standard.setValue(time, forKey: "startTime")
    }
    
    func getStartTime()->String{
        let time = UserDefaults.standard.string(forKey: "startTime")
        
        return time ?? "Not set"
    }
    
    func setEndTime(time: String){
        UserDefaults.standard.setValue(time, forKey: "endTime")
    }
    
    func getEndTime()->String{
        let time = UserDefaults.standard.string(forKey: "endTime")
        
        return time ?? "Not set"
    }
    
    
}

