//
//  Water.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 18/08/21.
//

import SwiftUI
import CoreData

class WaterViewModel: ObservableObject{
    
    @Published var waterLogs: [WaterLog] = []
    @Published var waterLog: WaterLog = WaterLog()
    
    @Environment(\.managedObjectContext) var context
    
    @FetchRequest(entity: WaterLog.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \WaterLog.date, ascending: false)], animation: .easeIn) var results : FetchedResults<WaterLog>
    
    
    
    func getCurrentWater() -> Double {
        let currentWater = UserDefaults.standard.double(forKey: "currentWater")
        return currentWater
    }
    
    func getWaterLogs() -> [WaterLog]{
        return PersistenceController.shared.getAllWaterLogs()
    }
    
    func addWater(size: Double){
        var currentWater = getCurrentWater()
        currentWater += size
        UserDefaults.standard.setValue(currentWater, forKey: "currentWater")
        
        let water = WaterLog(context: PersistenceController.shared.viewContext)
        water.date = Date()
        water.size = size
        PersistenceController.shared.save()

    }
    
    func getWaterPercentage(current: Double, goal: Double) -> Double {
        let percentage: Double = current/goal*100
        return percentage
    }
    
    func setWaterGoal(waterValue: Double){
        UserDefaults.standard.setValue(waterValue, forKey: "waterGoal")
        
    }
    
    func getWaterGoal() -> Double{
        
        if(UserDefaults.standard.double(forKey: "waterGoal") == 0){
            UserDefaults.standard.setValue(2000, forKey: "waterGoal")
        }
        let waterGoal = UserDefaults.standard.double(forKey: "waterGoal")
        
        return waterGoal
    }
    
    func resetCurrentWater(){
        UserDefaults.standard.setValue(0, forKey: "currentWater")
    }
    
    func deleteAllLogs(){
        
        // Get a reference to a NSPersistentStoreCoordinator
        var persistentContainer = PersistenceController().container
        
        let storeContainer =
            persistentContainer.persistentStoreCoordinator
        
        // Delete each existing persistent store
        for store in storeContainer.persistentStores {
            try? storeContainer.destroyPersistentStore(
                at: store.url!,
                ofType: store.type,
                options: nil
            )
        }

        // Re-create the persistent container
        persistentContainer = NSPersistentContainer(
            name: "Intake" // the name of
            // a .xcdatamodeld file
        )

        // Calling loadPersistentStores will re-create the
        // persistent stores
        persistentContainer.loadPersistentStores {
            (store, error) in
            // Handle errors
        }
    }
    
    func saveLastDate(date: Date){
        UserDefaults.standard.setValue(date.timeIntervalSince1970, forKey: "lastDate")
    }
    
    func getLastDate() -> Date {
        let date = Date(timeIntervalSince1970: UserDefaults.standard.double(forKey: "lastDate"))
        return date ?? Date()
    }
    
    func checkDay(){
        let lastDate = getLastDate()
        let check = Calendar.current.isDateInToday(lastDate)
        
        if(!check){
            resetCurrentWater()
            deleteAllLogs()
        }
    }
    
}

