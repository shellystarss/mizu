//
//  AddWater.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 18/08/21.
//

import SwiftUI

struct AddWater: View {
    @Environment(\.managedObjectContext) var context
    
    
    var body: some View {
        GeometryReader{reader in
            let rect = reader.frame(in: .global)
            VStack(spacing:15){
                HStack(spacing:15){
                    WaterButton(rect: rect, image: "cup", title: "150ml")
                    WaterButton(rect: rect, image: "glass", title: "200ml")
                    
                }.frame(width: rect.width, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
                HStack(spacing:15){
                    WaterButton(rect: rect, image: "mug", title: "250ml")
                    WaterButton(rect: rect, image: "bottle", title: "500ml")
                    
                    
                }.frame(width: rect.width, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
            }.padding(.top,15)
            
        }
    }
}

struct AddWater_Previews: PreviewProvider {
    static var previews: some View {
        AddWater()
    }
}

struct WaterButton: View{
    @Environment(\.presentationMode) var presentation
    @ObservedObject var waterVM = WaterViewModel()
    @State var rect: CGRect
    @State var image: String
    @State var title: String
    
    
    var body: some View{
        
            Button(action: {
                if(image=="cup"){
                    waterVM.addWater(size: 150)
                }else if(image=="glass"){
                    waterVM.addWater(size: 200)
                }else if(image=="mug"){
                    waterVM.addWater(size: 250)
                }else if(image=="bottle"){
                    waterVM.addWater(size: 500)
                }
                presentation.wrappedValue.dismiss()
            }, label: {
                VStack{
                    Image(image)
                        .resizable()
                        .scaledToFit()
                    Text(title)
                        .font(.system(size: 10))
                }
            })
            
            .buttonStyle(PlainButtonStyle())
            .padding()
            .frame(width: rect.width/2.5, height: rect.height/3.5, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .background(Color.blue)
            .clipShape(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)))
        }
    
    
}

struct NavButton: View{
    var image: String
    var title: String
    var rect: CGRect
    
    var body: some View{
        VStack(){
            Image(systemName: image)
                .colorInvert()
                .font(.title2)
                .frame(width: rect.width / 4 , height: rect.width / 4, alignment: .center)
            
            
            Text(title)
                .font(.system(size: 10))
                .foregroundColor(.white)
        }
    }
}

