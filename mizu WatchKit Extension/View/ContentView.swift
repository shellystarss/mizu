//
//  ContentView.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 16/08/21.
//

import SwiftUI
struct ContentView: View {
    @State var toggle = false
    
    init(){
        
        WaterViewModel().checkDay()
        WaterViewModel().saveLastDate(date: Date())
    }
    
    var body: some View {
        
            TabView{
                
                ZStack{
                        
                    Color.backgroundColor.ignoresSafeArea(.all)
                    WaterProgress()
                }
                ZStack{
                        
                    Color.backgroundColor.ignoresSafeArea(.all)
                    WaterGoal()
                }
                ZStack{
                        
                    Color.backgroundColor.ignoresSafeArea(.all)
                    ReminderSetting()
                }
                
                
                
                
            }
            .tabViewStyle(PageTabViewStyle())
            .onAppear(){
                WaterViewModel().checkDay()
                WaterViewModel().saveLastDate(date: Date())
                self.toggle.toggle()
            }
        
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
