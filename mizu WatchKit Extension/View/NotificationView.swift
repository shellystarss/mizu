//
//  NotificationView.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 16/08/21.
//

import SwiftUI
import UserNotifications

struct NotificationView: View {
    var current = WaterViewModel().getCurrentWater()
    var goal = WaterViewModel().getWaterGoal()
    
    func getTimeNow() -> String{
        let time = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mma"
        return dateFormatter.string(from: time)
    }
    
    var body: some View {
        ZStack{
                
            Color.backgroundColor.ignoresSafeArea(.all)
            ScrollView{
            Image("water-drop").resizable()
                .frame(width: 30, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .padding()
            Text("It's time to drink water!")
                .font(.headline)
            Divider().padding()
                
            Text("\(goal-current, specifier: "%.0f") ml more for today")
                .font(.caption2)
                .frame(width: 130, alignment: .center)
                
                Text("\(getTimeNow())").font(.footnote).padding(20)
        }
            
        }
        
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
