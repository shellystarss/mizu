//
//  Reminder.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 18/08/21.
//

import SwiftUI

struct ReminderSetting: View {
    @ObservedObject var waterVM = WaterViewModel()
    @State var isOn: Bool = WaterViewModel().getWaterReminderStatus()
    @State var timeModal = false
    @State var startModal = false
    @State var endModal = false
    
    var body: some View {
       
        VStack(alignment:.leading, spacing:10){
            Toggle(isOn: $isOn, label: {
                Text("Reminder")
            })
            .toggleStyle(SwitchToggleStyle(tint: .blue))
            .onChange(of: isOn){_isOn in
                waterVM.setWaterReminderStatus(isOn: _isOn)
            }
            if(isOn){
                
                ScrollView{
                    Button(action: {
                        self.timeModal.toggle()
                    }, label: {
                        
                            WaterMenu( key: "Time interval", value: "1h")
                        
                            
                    })
                    .sheet(isPresented: $timeModal, content: {
                        TimeModal()
                    })
                    
                    Button(action: {
                        self.startModal.toggle()
                    }, label: {
                        
                            Color.backgroundColor.ignoresSafeArea(.all)
                            WaterMenu( key: "Start time", value: "7:30")
                        
                            
                    })
                    .sheet(isPresented: $startModal, content: {
                        StartModal()
                    })
                    
                    Button(action: {
                        self.endModal.toggle()
                    }, label: {
                        
                            Color.backgroundColor.ignoresSafeArea(.all)
                            WaterMenu( key: "End Time", value: "22:00")
                        
                            
                    })
                    .sheet(isPresented: $endModal, content: {
                        EndModal()
                    })
                    
//
                }
            }else{
                Spacer()
            }
          
        }.padding()
        
        
    }
}

struct WaterMenu: View{
    
    @State var key: String
    @State var value: String
    var body: some View{
        
            HStack{
                Text(key)
                    .font(.system(size: 12))
                    .frame(width: 90, height: 20, alignment: .leading)
                HStack{
                    Text(value)
                    Image(systemName: "chevron.right")
                        .resizable()
                        .frame(width: 5, height: 8, alignment: .trailing)
                }
                    .font(.system(size: 12))
                    .foregroundColor(Color.gray)
                    .frame(width: 50, height: 20, alignment: .trailing)
                    
            }
                
            
        
    }
}

struct TimeModal: View{
    var timeInterval = ["20 minutes", "30 minutes", "45 minutes", "1 hour", "1 hour 30 minutes", "2 hours"]
    @State var selectedTime = "1 hour"
    var body: some View{
       
            Picker("", selection: $selectedTime) {
                ForEach(timeInterval, id: \.self) {i in
                    if(i == selectedTime){
                        Text(i)
                            .foregroundColor(.blue)
                    }else{
                        Text(i)
                            .foregroundColor(.white)
                    }
                    
                }
                
            }
            .focusBorderHidden()
            .pickerStyle(WheelPickerStyle())
        
    }
}

struct StartModal: View{
    var body: some View{
        Text("Start")
    }
}

struct EndModal: View{
    var body: some View{
        Text("End")
    }
}

struct Reminder_Previews: PreviewProvider {
    static var previews: some View {
        ReminderSetting()
    }
}
