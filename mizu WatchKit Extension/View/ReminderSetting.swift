//
//  Reminder.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 18/08/21.
//

import SwiftUI
import UserNotifications

struct ReminderSetting: View {
    @ObservedObject var reminderVM = ReminderViewModel()
    @State var isOn: Bool = ReminderViewModel().getWaterReminderStatus()
    @State var timeModal = false
    @State var startModal = false
    @State var endModal = false
    
    @State var timeInterval = ReminderViewModel().getTimeInterval()
    @State var startTime = ReminderViewModel().getStartTime()
    @State var endTime = ReminderViewModel().getEndTime()
    
    @State var reminder = ReminderViewModel().makeNotification()
    @State var toggle = false
    
   
    
    var body: some View {
       
        VStack(alignment:.leading, spacing:10){
            Toggle(isOn: $isOn, label: {
                Text("Reminder")
            })
            .toggleStyle(SwitchToggleStyle(tint: .blue))
            .onChange(of: isOn){_isOn in
                
                reminderVM.setWaterReminderStatus(isOn: _isOn)
            }
            if(isOn){
                
                ScrollView{
                    
                    
                    Button(action: {
                        self.startModal.toggle()
                    }, label: {
                        
                            Color.backgroundColor.ignoresSafeArea(.all)
                        WaterMenu( key: "Start time", value: $startTime)
                        
                            
                    })
                    .sheet(isPresented: $startModal, content: {
                        TimeModal(isStart: true)
                    })
                    
                    Button(action: {
                        self.endModal.toggle()
                    }, label: {
                        
                            Color.backgroundColor.ignoresSafeArea(.all)
                            WaterMenu( key: "End Time", value: $endTime)
                        
                            
                    })
                    .sheet(isPresented: $endModal, content: {
                        TimeModal(isStart: false)
                    })
                    
                    
                    Button(action: {
                        self.timeModal.toggle()
                    }, label: {
                        WaterMenu( key: "Time interval", value: $timeInterval)
                    })
                    .sheet(isPresented: $timeModal, content: {
                        IntervalModal()
                    })
                    
                }
            }else{
                Spacer()
            }
          
        }.padding()
        .onAppear(){
            timeInterval = ReminderViewModel().getTimeInterval()
            startTime = ReminderViewModel().getStartTime()
            endTime = ReminderViewModel().getEndTime()
            reminderVM.setWaterReminderStatus(isOn: isOn)
            toggle.toggle()
        }
        
    }
}

struct WaterMenu: View{
    
    @State var key: String
    @Binding var value: String
    var body: some View{
        
            HStack{
                Text(key)
                    .font(.system(size: 12))
                    .frame(width: 75, height: 20, alignment: .leading)
                HStack{
                    Text(value)
                    Image(systemName: "chevron.right")
                        .resizable()
                        .frame(width: 5, height: 8, alignment: .trailing)
                }
                    .font(.system(size: 12))
                    .foregroundColor(Color.gray)
                    .frame(width: 65, height: 20, alignment: .trailing)
                    
            }
                
            
        
    }
}

struct IntervalModal: View{
    
    @Environment(\.presentationMode) var presentation
    @ObservedObject var reminderVM = ReminderViewModel()
    var timeInterval = ["20 minutes", "30 minutes", "45 minutes", "1 hour", "1 hour 30 minutes", "2 hours"]
    @State var selectedTime = ReminderViewModel().convertTime()
    
    var body: some View{
        VStack{
            Picker("", selection: $selectedTime) {
                ForEach(timeInterval, id: \.self) {i in
                    if(i == selectedTime){
                        Text(i)
                            .foregroundColor(.blue)
                    }else{
                        Text(i)
                            .foregroundColor(.white)
                    }
                    
                }
                
            }
            .focusBorderHidden()
            .pickerStyle(WheelPickerStyle())
            
                
                Button("OK"){
                    
                    reminderVM.setTimeInterval(interval: selectedTime)
                    //reminderVM.setNotificationTime()
                    
                    presentation.wrappedValue.dismiss()
                }.buttonStyle(BorderedButtonStyle())
            
            
        }
    }
}

struct TimeModal: View{
    @Environment(\.presentationMode) var presentation
    @ObservedObject var reminderVM = ReminderViewModel()
    
    @State var isStart: Bool
    
    @State var selectedHour = 1
    @State var selectedMin = 0
    @State var selectedTime = "AM"
    
    func getTime(i: Int) -> String{
        var time: String
        if(-1 < i && i < 10){
            time = "0\(i)"
        }else{
            time = "\(i)"
        }
        
        return time
    }
    
    var hours = [Int](1..<13)
    var minutes = [Int](0..<60)
    var time = ["AM", "PM"]
    
    var body: some View{
        GeometryReader{geo in
            VStack{
                HStack{
                    Picker("", selection: $selectedHour) {
                        ForEach(hours, id: \.self) {i in
                            if(i == selectedHour){
                                Text("\(i)")
                                    .foregroundColor(.blue)
                            }else{
                                Text("\(i)")
                                    .foregroundColor(.white)
                            }
                            
                        }
                        
                    }
                    .focusBorderHidden()
                    .pickerStyle(WheelPickerStyle())
                    .frame(width:geo.size.width / 3.3 )
                    
                    Picker("", selection: $selectedMin) {
                        ForEach(minutes, id: \.self) {i in
                            
                            if(i == selectedMin){
                                Text(getTime(i: i))
                                    .foregroundColor(.blue)
                            }else{
                                Text(getTime(i: i))
                                    .foregroundColor(.white)
                            }
                            
                        }
                        
                    }
                    .focusBorderHidden()
                    .pickerStyle(WheelPickerStyle())
                    .frame(width:geo.size.width / 3.3 )
                    
                    Picker("", selection: $selectedTime) {
                        ForEach(time, id: \.self) {i in
                            
                            if(i == selectedTime){
                                Text("\(i)")
                                    .foregroundColor(.blue)
                            }else{
                                Text("\(i)")
                                    .foregroundColor(.white)
                            }
                            
                        }
                        
                    }
                    .focusBorderHidden()
                    .pickerStyle(WheelPickerStyle())
                    .frame(width:geo.size.width / 3.3 )
                }
                    Button("OK"){
                        var min: String
                        if(-1 < selectedMin && selectedMin < 10){
                            min = "0\(selectedMin)"
                        }else{
                            min = "\(selectedMin)"
                        }
                        let time = "\(selectedHour):\(min)\(selectedTime)"
                        if(isStart){
                            reminderVM.setStartTime(time: time)
                        }else{
                            reminderVM.setEndTime(time: time)
                        }
                        //reminderVM.setNotificationTime()
                        presentation.wrappedValue.dismiss()
                    }.buttonStyle(BorderedButtonStyle())
            }
        }
    }
}

struct Reminder_Previews: PreviewProvider {
    static var previews: some View {
        ReminderSetting()
    }
}
