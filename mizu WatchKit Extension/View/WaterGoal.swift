//
//  WaterGoal.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 18/08/21.
//

import SwiftUI

struct WaterGoal: View {
    @ObservedObject var waterVM = WaterViewModel()
    @State private var waterValue = WaterViewModel().getWaterGoal()
    var minValue = 1000.0
    var maxValue = 5000.0
    var step = 100.0
    
    var body: some View {
        VStack(spacing:10){
            Text("Water goal")
                .font(.system(size: 20))
            
            Slider(value: $waterValue, in: self.minValue...self.maxValue, step: self.step)
                .focusBorderHidden()
            Text("\(waterValue, specifier: "%.0f")ml")
                .font(.system(size: 14))
            }.padding()
        .onDisappear(){
            waterVM.setWaterGoal(waterValue: waterValue)
        }
    }
}

struct WaterGoal_Previews: PreviewProvider {
    static var previews: some View {
        WaterGoal()
    }
}
