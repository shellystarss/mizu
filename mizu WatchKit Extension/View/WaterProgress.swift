//
//  WaterProgress.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 18/08/21.
//

import SwiftUI

struct WaterProgress: View {
    @State private var isOn: Bool = false
    
    @ObservedObject private var waterVM = WaterViewModel()
    
    
    var body: some View {
        
        ScrollView{
            
            ZStack{
                Pulsation()
                Track()
                VStack{
                    PercentageLabel()
                    NavigationLink(
                        destination: AddWater(),
                        label: {
                            Text("+")
                                .background(
                                    Circle()
                                        .fill(Color.blue)
                                        .frame(width:25, height: 25)
                                )
                                .padding(.top,5)
                            
                        })
                        .buttonStyle(PlainButtonStyle())
                }
                
                Outline()
                
                
            }
            .frame(width: 170, height: 170, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .padding(.top,10)
            WaterLogs()
        }
        
    }
}

struct PercentageLabel: View{
    @State var refresh: Bool = false
    @State var percentage = WaterViewModel().getWaterPercentage(current: WaterViewModel().getCurrentWater(), goal: WaterViewModel().getWaterGoal())
    @State var current = WaterViewModel().getCurrentWater()
    @State var goal = WaterViewModel().getWaterGoal()
    
    var body: some View{
        ZStack{
            VStack{
                Text("\(String(format: "%.0f",percentage))%")
                    .font(.system(size: 24))
                    .fontWeight(.bold)
                Text("\(current, specifier: "%.0f")/\(goal, specifier: "%.0f")ml")
                    .font(.system(size: 10))
                    .foregroundColor(.gray)
            }
            
        }.onAppear(){
            percentage = WaterViewModel().getWaterPercentage(current: WaterViewModel().getCurrentWater(), goal: WaterViewModel().getWaterGoal())
            current = WaterViewModel().getCurrentWater()
            goal = WaterViewModel().getWaterGoal()
            self.refresh.toggle()
        }
    }
}

struct Outline: View{
    @State private var outline = false
    var waterVM = WaterViewModel()
    @State var percentage: Double = 0
    var colors: [Color] = [Color.blue, Color.outlineColor]
    var body: some View{
        ZStack{
            Circle()
                .fill(Color.clear)
                .frame(width:120, height: 120)
                .overlay(
                    Circle()
                        .trim(from: 0, to: CGFloat(percentage*0.01))
                        .stroke(style: StrokeStyle(lineWidth: 10, lineCap: .round, lineJoin: .round))
                        .fill(AngularGradient(gradient: .init(colors: colors), center: .center, startAngle: .init(degrees: 0), endAngle: .init(degrees: 350)))
                        .rotationEffect(Angle(degrees: 270))
                )
                .animation(.spring(response: 2.0, dampingFraction: 1.0, blendDuration: 1.0))
                .onAppear(){
                    self.percentage = waterVM.getWaterPercentage(current: waterVM.getCurrentWater(), goal: waterVM.getWaterGoal())
                    self.outline.toggle()
                }
                .onDisappear(){
                    self.percentage = 0
                }
            
        }
    }
}

struct Track: View{
    var colors: [Color] = [Color.trackColor]
    var body: some View{
        ZStack{
            Circle()
                .fill(Color.backgroundColor)
                .frame(width: 120, height: 120)
                .overlay(
                    Circle()
                        .stroke(style: StrokeStyle(lineWidth:15))
                        .fill(AngularGradient(gradient: .init(colors: colors), center: .center))
                )
        }
        
    }
}

struct Pulsation: View{
    @State private var pulsate = false
    var colors: [Color] = [Color.pulsatingColor]
    var body: some View{
        ZStack{
            Circle()
                .fill(Color.pulsatingColor)
                .frame(width:120, height:120)
                .scaleEffect(pulsate ? 1.3:1.1)
                .animation(Animation.easeInOut(duration: 1.1).repeatForever(autoreverses: true))
                .onAppear(){
                    self.pulsate.toggle()
                }
            
        }
    }
}


struct WaterLogs: View{
    @State var refresh: Bool = false
    @State var waterLogs : [WaterLog] = WaterViewModel().getWaterLogs()
    var body: some View{
        VStack{
            
            Text("Today's Intake (\(waterLogs.count))")
                .padding(.top)
            ScrollView{
                ForEach(waterLogs, id:\.self){wl in
                    
                    
                    Button(action: {}, label:{
                        HStack{
                            
                            Text("\(wl.size, specifier: "%.0f")ml")
                                .font(.system(size: 12))
                                .frame(width: 60, alignment: .leading)
                            
                            Text(wl.date!, style: .time)
                                .font(.system(size: 12))
                                .foregroundColor(.gray)
                                .frame(width: 60, alignment: .trailing)
                            
                        }
                        
                    })
                }
                
            }.padding()
            
        }
        .onAppear(){
            
            waterLogs = PersistenceController.shared.getAllWaterLogs()
            self.refresh.toggle()
        }
        
    }
}

struct WaterProgress_Previews: PreviewProvider {
    static var previews: some View {
        WaterProgress()
    }
}
