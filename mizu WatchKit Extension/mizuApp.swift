//
//  mizuApp.swift
//  mizu WatchKit Extension
//
//  Created by Shelina Linardi on 16/08/21.
//

import SwiftUI

@main
struct mizuApp: App {
    let container = PersistenceController.shared.container
    
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
            .environment(\.managedObjectContext, container.viewContext)
        }

        #if os(watchOS)
        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
        #endif
    }
}
